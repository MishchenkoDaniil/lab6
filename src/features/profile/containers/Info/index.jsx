import { motion } from 'framer-motion';
import Card from '@mui/material/Card';
import { useDispatch, useSelector } from 'react-redux';

import * as profileThunks from 'features/profile/thunks/profile';

import ROLES from 'features/common/enums';
import ProfileForm from './ProfileForm';
import ChangeAvatarForm from './ChangeAvatarForm';
import RemoveForm from './RemoveForm';

import BuyerGeneralForm from './BuyerForm/GeneralForm';
import SellerGeneralForm from './SellerProfile/GeneralForm';
import ChangePasswordForm from './ChangePassword';

const container = {
  show: {
    transition: {
      staggerChildren: 0.05,
    },
  },
};

const item = {
  hidden: { opacity: 0, y: 40 },
  show: { opacity: 1, y: 0 },
};

function InfoProfile() {
  const { user, role } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const handleSubmitProfileForm = (values) => {
    dispatch(profileThunks.updateProfileThunk(values));
  };

  const handleDeleteAvatar = () => {
    dispatch(profileThunks.deleteAvatarThunk());
  };

  const handleSubmitSellerGeneralInfo = (values) => {
    dispatch(profileThunks.updateSellerProfileThunk(values));
  };

  const handleSubmitBuyerGeneralInfo = (values) => {
    dispatch(profileThunks.updateBuyerProfileThunk(values));
  };

  const handleSubmitAvatar = (e) => {
    dispatch(profileThunks.uploadAvatarThunk(e.target.files[0]));
  };

  const handleSubmitPassword = (values) => {
    dispatch(profileThunks.changePasswordThunk(values));
  };

  const handleRemoveProfile = () => {};

  return (
    <motion.div variants={container} initial="hidden" animate="show">
      <Card component={motion.div} variants={item} className="w-full mb-32 p-20 rounded-16 shadow">
        <ChangeAvatarForm
          photo={user?.photo}
          onUpload={handleSubmitAvatar}
          onDelete={handleDeleteAvatar}
        />
      </Card>
      <Card component={motion.div} variants={item} className="w-full mb-32 p-20 rounded-16 shadow">
        {role === ROLES.Seller ? (
          <SellerGeneralForm data={user} onSubmit={handleSubmitSellerGeneralInfo} />
        ) : (
          <BuyerGeneralForm data={user} onSubmit={handleSubmitBuyerGeneralInfo} />
        )}
      </Card>
      <Card component={motion.div} variants={item} className="w-full mb-32 p-20 rounded-16 shadow">
        <ProfileForm data={user} onSubmit={handleSubmitProfileForm} />
      </Card>
      <Card component={motion.div} variants={item} className="w-full mb-32 p-20 rounded-16 shadow">
        <ChangePasswordForm onSubmit={handleSubmitPassword} />
      </Card>
      <Card component={motion.div} variants={item} className="w-full mb-32 p-20 rounded-16 shadow">
        <RemoveForm onSubmit={handleRemoveProfile} />
      </Card>
    </motion.div>
  );
}

InfoProfile.displayName = 'InfoProfile';

export default InfoProfile;
