import PropTypes from 'prop-types';
import Icon from '@mui/material/Icon';
import IconButton from '@mui/material/IconButton';
import { useDispatch } from 'react-redux';
import { toggleQuickPanel } from './store/stateSlice';

function QuickPanelToggleButton({ children }) {
  const dispatch = useDispatch();

  return (
    <IconButton className="w-40 h-40" onClick={() => dispatch(toggleQuickPanel())} size="large">
      {children}
    </IconButton>
  );
}

QuickPanelToggleButton.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

QuickPanelToggleButton.defaultProps = {
  children: <Icon>bookmarks</Icon>,
};

export default QuickPanelToggleButton;
