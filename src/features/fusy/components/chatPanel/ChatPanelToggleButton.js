import PropTypes from 'prop-types';
import Icon from '@mui/material/Icon';
import IconButton from '@mui/material/IconButton';
import { useDispatch } from 'react-redux';
import { toggleChatPanel } from './store/stateSlice';

function ChatPanelToggleButton({ children }) {
  const dispatch = useDispatch();

  return (
    <IconButton className="w-40 h-40" onClick={() => dispatch(toggleChatPanel())} size="large">
      {children}
    </IconButton>
  );
}

ChatPanelToggleButton.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

ChatPanelToggleButton.defaultProps = {
  children: <Icon>chat</Icon>,
};

export default ChatPanelToggleButton;
