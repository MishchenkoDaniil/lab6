import classNames from 'classnames/bind';

import paths from 'app/Routes/paths';

import FuseNavigation from '@fuse/core/FuseNavigation/FuseNavigation';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

const navigationConfig = [
  {
    id: 'users',
    title: 'Users',
    type: 'item',
    url: paths.users(),
  },
  {
    id: 'listings',
    title: 'Listings',
    type: 'item',
    url: paths.listings(),
  }
];

function AdminDrawerList() {
  return (
    <FuseNavigation
      layout="vertical"
      className={cx('admin-drawer-list')}
      navigation={navigationConfig}
    />
  );
}
AdminDrawerList.displayName = 'AdminDrawerList';

AdminDrawerList.propTypes = {};

export default AdminDrawerList;
