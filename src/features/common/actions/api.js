import { REQUEST_END, REQUEST_START } from '../actionTypes/api';

const BASE_ROUTE = 'api/';

export const requestStartAction = () => ({
  type: BASE_ROUTE + REQUEST_START,
});

export const requestEndAction = () => ({
  type: BASE_ROUTE + REQUEST_END,
});
