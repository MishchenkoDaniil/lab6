import PropTypes from 'prop-types';
import { ThemeProvider, styled } from '@mui/material/styles';

import Box from '@mui/material/Box';

import clsx from 'clsx';
import MuiDrawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';

import AppBar from '@mui/material/AppBar';
import classNames from 'classnames/bind';
import Logo from 'assets/images/logos/logo-alhamize.png';

import UserNavbarHeader from 'features/fusy/components/UserNavbarHeader';
import { useSelector } from 'react-redux';
import { selectNavbarTheme } from 'features/common/slices/fuse/settingsSlice';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

const drawerWidth = 280;

const navbarWidth = 280;

const StyledNavBar = styled('div')(({ theme, open, position }) => ({
  minWidth: navbarWidth,
  width: navbarWidth,
  maxWidth: navbarWidth,
  ...(!open && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.leavingScreen,
    }),
    ...(position === 'left' && {
      marginLeft: `-${navbarWidth}px`,
    }),
    ...(position === 'right' && {
      marginRight: `-${navbarWidth}px`,
    }),
  }),
  ...(open && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(9)} + 1px)`,
  },
});

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: 'nowrap',
  boxSizing: 'border-box',
  ...(open && {
    ...openedMixin(theme),
    '& .MuiDrawer-paper': openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    '& .MuiDrawer-paper': closedMixin(theme),
  }),
}));

const Root = styled('div')(({ theme }) => ({
  'backgroundColor': theme.palette.background.default,
  'color': theme.palette.text.primary,
  '& ::-webkit-scrollbar-thumb': {
    boxShadow: `inset 0 0 0 20px ${
      theme.palette.mode === 'light' ? 'rgba(0, 0, 0, 0.24)' : 'rgba(255, 255, 255, 0.24)'
    }`,
  },
  '& ::-webkit-scrollbar-thumb:active': {
    boxShadow: `inset 0 0 0 20px ${
      theme.palette.mode === 'light' ? 'rgba(0, 0, 0, 0.37)' : 'rgba(255, 255, 255, 0.37)'
    }`,
  },
}));

function HomeTemplate({ children, DrawerChildren, HeaderChildren, open = false }) {
  const navbarTheme = useSelector(selectNavbarTheme);

  return (
    <ThemeProvider theme={navbarTheme}>
      <Box sx={{ display: 'flex', width: '100%' }}>
        <CssBaseline />
        {HeaderChildren}
        <Drawer variant="permanent" open={open} className={cx('drawer-list')}>
          <StyledNavBar
            open={open}
            className="flex-col flex-auto sticky top-0 overflow-hidden h-screen flex-shrink-0 z-20 shadow-5"
          >
            <Root className={clsx('flex flex-auto flex-col overflow-hidden h-full')}>
              <AppBar
                color="primary"
                position="static"
                className="flex flex-row items-center flex-shrink h-48 md:h-64 min-h-48 md:min-h-64 px-12 shadow-0"
              >
                <div className="flex flex-1 mx-24 pt-20">
                  <img alt="logo" src={Logo} className={cx('logo')} />
                </div>
              </AppBar>

              <UserNavbarHeader />

              {DrawerChildren}
            </Root>
          </StyledNavBar>
        </Drawer>

        <Box
          component="main"
          sx={{
            flexGrow: 1,
            p: 3,
            display: 'flex',
            flexDirection: 'column',
            overflowY: 'auto',
          }}
        >
          <DrawerHeader />
          {children}
        </Box>
      </Box>
    </ThemeProvider>
  );
}

HomeTemplate.displayName = 'HomeTemplate';

HomeTemplate.propTypes = {
  children: PropTypes.node.isRequired,
  DrawerChildren: PropTypes.node.isRequired,
  HeaderChildren: PropTypes.node.isRequired,
  open: PropTypes.bool.isRequired,
};

export default HomeTemplate;
