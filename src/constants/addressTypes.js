const ADDRESS_TYPES = {
  HOME: 'home',
  MAILING: 'mailing',
  EMPLOYER: 'employer',
};

export default ADDRESS_TYPES;
