import compose from 'compose-function';

import withFuse from './withFuse';
import withRedux from './withRedux';
import withStyledEngine from './withStyledEngine';
import withRouter from './withRouter';
import withAuth from './withAuth';

const withProviders = compose(withStyledEngine, withRouter, withRedux, withAuth, withFuse);

export default withProviders;
