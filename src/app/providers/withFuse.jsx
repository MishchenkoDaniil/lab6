import FuseLayout from '@fuse/core/FuseLayout';
import FuseTheme from '@fuse/core/FuseTheme';

const withFuse = (Component) =>
  function (props) {
    return (
      <FuseTheme>
        <Component {...props} />
        <FuseLayout />
      </FuseTheme>
    );
  };

export default withFuse;
