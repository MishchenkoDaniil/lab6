import { Routes as RoutesBase, Route, Outlet, Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import BuyerLayout from 'features/common/Layouts/BuyerLayout';
import SellerLayout from 'features/common/Layouts/SellerLayout';

import PrivateRoute from 'features/common/route-guards/PrivateRoute';
import PublicRoute from 'features/common/route-guards/PublicRoute';
import BuyerRoute from 'features/common/route-guards/BuyerRoute';
import SellerRoute from 'features/common/route-guards/SellerRoute';
import AdminRoute from 'features/common/route-guards/AdminRoute';
import { selectCurrentUserRole } from 'features/common/slices/auth';

import SellerHomePage from 'pages/seller/seller-home';
import BuyerHomePage from 'pages/buyer/buyer-home';
import SignInPage from 'pages/join/sign-in';
import ForgotPasswordPage from 'pages/reset-password';
import SignUpPage from 'pages/join/sign-up';
import LogoutPage from 'pages/logout';
import ProfilePage from 'pages/profile';
import NotFoundPage from 'pages/404';

import GeneralLayouts from 'shared/layouts/GeneralLayouts';
import ROLES from 'features/common/enums';
import paths from './paths';
import { AddListingPage } from 'pages/seller/add-listing/page';
import CurrentListingsPage from 'pages/seller/current-listings';
import ListingDetailsPage from 'pages/seller/listing-details';
import AdminLayout from 'features/common/Layouts/AdminLayout/index';
import UsersPage from 'pages/admin/users';
import ListingsPage from 'pages/admin/listings';

function Layout() {
  return (
    <GeneralLayouts>
      <Outlet />
    </GeneralLayouts>
  );
}

function HomeRoutesWithLayouts() {
  const role = useSelector(selectCurrentUserRole);

  if (role === ROLES.Seller) {
    return <Navigate replace to={paths.seller()} />;
  } else if (role === ROLES.Buyer) {
    return <Navigate replace to={paths.buyer()} />;
  } else {
    return <Navigate replace to={paths.admin()}/>;
  }

  
}

function SellerRoutes() {
  return (
    <SellerLayout>
      <Outlet />
    </SellerLayout>
  );
}

function BuyerRoutes() {
  return (
    <BuyerLayout>
      <Outlet />
    </BuyerLayout>
  );
}

function AdminRoutes() {
  return (
    <AdminLayout>
      <Outlet />
    </AdminLayout>
  )
}

function Routes() {
  return (
    <RoutesBase>
      <Route element={<Layout />}>
        <Route path={paths.home()} element={<PrivateRoute component={HomeRoutesWithLayouts} />} />
        <Route path={paths.admin()} element={<AdminRoutes />}>
          <Route path={paths.admin()} element={<AdminRoute component={() => <div>Home</div>} />} />
          <Route path={paths.users()} element={<AdminRoute component={UsersPage} />} />
          <Route path={paths.listings()} element={<AdminRoute component={ListingsPage} />} />
          <Route />
          <Route path="*" element={<AdminRoute component={() => <div>Home</div>} />} />
        </Route>
        <Route path={paths.seller()} element={<SellerRoutes />}>
          <Route path={paths.seller()} element={<SellerRoute component={SellerHomePage} />} />
          <Route path={paths.sellerProfile()} element={<SellerRoute component={ProfilePage} />} />
          <Route path={paths.addListing()} element={<SellerRoute component={AddListingPage} />} />
          <Route path={paths.editListing()} element={<SellerRoute component={AddListingPage} />} />
          <Route path={paths.sellerListingInfo()} element={<SellerRoute component={ListingDetailsPage} />} />
          <Route path={paths.currentListings()} element={<SellerRoute component={CurrentListingsPage} />} />
          <Route />
          <Route path="*" element={<SellerRoute component={SellerHomePage} />} />
        </Route>
        <Route path={paths.buyer()} element={<BuyerRoutes />}>
          <Route path={paths.buyer()} element={<BuyerRoute component={BuyerHomePage} />} />
          <Route path={paths.buyerProfile()} element={<BuyerRoute component={ProfilePage} />} />
          <Route path={paths.buyerListingInfo()} element={<BuyerRoute component={ListingDetailsPage} />} />
          <Route path={paths.searchListings()} element={<BuyerRoute component={CurrentListingsPage} />} />
          <Route path="*" element={<BuyerRoute component={BuyerHomePage} />} />
        </Route>
        <Route path={paths.signIn()} element={<PublicRoute component={SignInPage} />} />
        <Route path={paths.signUp()} element={<PublicRoute component={SignUpPage} />} />
        <Route path={paths.logout()} element={<PrivateRoute component={LogoutPage} />} />
        <Route path={paths.forgotPassword()} element={<PublicRoute component={ForgotPasswordPage} />} />
        <Route path="*" element={<NotFoundPage />} />
      </Route>
    </RoutesBase>
  );
}

export default Routes;
