import { Helmet } from 'react-helmet';
import Charts from 'features/buyer/containers/charts/index';

/* eslint-disable import/prefer-default-export */
export function BuyerHomePage() {
  return (
    <>
      <Helmet>
        <title>Alphamize | Dashboard</title>
      </Helmet>
      <Charts />
    </>
  );
}

BuyerHomePage.displayName = 'BuyerHomePage';
