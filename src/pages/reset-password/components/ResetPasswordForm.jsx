import { useFormik } from 'formik';
import { useSelector } from 'react-redux';
import { TextField, Grid } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { checkIfIsLoading } from 'features/common/slices/api/index';

const validationSchema = Yup.object().shape({
  email: Yup.string('Enter your email')
    .when('token', {
      is: (token) => !token,
      then: Yup.string().email('Enter a valid email'),
    })
    .required('Email is required'),
  password: Yup.string('Enter your password').when('token', {
      is: token => !!token,
      then: Yup.string().min(8, 'Password should be of minimum 8 characters length')
      .required('Password is required') 
  }),
  passwordConfirmation: Yup.string('Confirm your password').when('token', {
    is: token => !!token,
    then: Yup.string().min(8)
    .oneOf([Yup.ref('password'), null], 'Passwords must match')
    .required('Required')
    .trim() 
}),
});

function ResetPasswordForm({ onSubmit }) {
  const isLoading = useSelector(checkIfIsLoading);
  const formik = useFormik({
    initialValues: {
      email: new URLSearchParams(window.location.search).get('email') || '',
      password: '',
      passwordConfirmation: '',
      token: new URLSearchParams(window.location.search).get('token'),
    },
    validationSchema,
    validateOnMount: false,
    validateOnChange: false,
    onSubmit,
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container columns={12} rowSpacing={4}>
        {!formik.values.token && (
          <Grid item xs={12}>
            <TextField
              name="email"
              label="Email"
              type="email"
              fullWidth
              error={formik.errors.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
              onChange={formik.handleChange}
              value={String(formik.values?.email)}
            />
          </Grid>
        )}
        {formik.values.token && (
          <>
            <Grid item xs={12}>
              <TextField
                name="password"
                label="New password"
                fullWidth
                type="password"
                error={formik.errors.password && Boolean(formik.errors.password)}
                helperText={formik.touched.password && formik.errors.password}
                onChange={formik.handleChange}
                value={formik.values.password}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name="passwordConfirmation"
                label="Confirm new password"
                type="password"
                fullWidth
                error={
                  formik.errors.passwordConfirmation && Boolean(formik.errors.passwordConfirmation)
                }
                helperText={
                  formik.touched.passwordConfirmation && formik.errors.passwordConfirmation
                }
                onChange={formik.handleChange}
                value={formik.values.passwordConfirmation}
              />
            </Grid>
          </>
        )}
        <Grid item xs={12} className="flex justify-end">
          <LoadingButton loading={isLoading} color="secondary" variant="contained" type="submit">
            {formik.values.token ? 'Change password' : 'Reset Password'}
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
}

ResetPasswordForm.displayName = 'ResetPasswordForm';

ResetPasswordForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default ResetPasswordForm;
