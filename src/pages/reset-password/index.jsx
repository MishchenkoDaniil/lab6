import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';

const ForgotPasswordPage = loadable(() => import(/* webpackChunkName: "join-page" */ './page'), {
  resolveComponent: ({ ResetPasswordPage: Page }) => Page,
  fallback: <Loading />,
});

export default ForgotPasswordPage;
