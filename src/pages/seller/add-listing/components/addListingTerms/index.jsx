import PropTypes from 'prop-types';
import { useState } from 'react';
import { TextField, Grid, Typography, Button, Card, CardContent } from '@mui/material';
import FormHelperText from '@mui/material/FormHelperText';

import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import LoadingButton from '@mui/lab/LoadingButton';
import InputAdornment from '@mui/material/InputAdornment';
import { useDispatch, useSelector } from 'react-redux';
import { getContractPreviewThunk } from 'features/common/thunks/listing';
import { checkIfIsLoading } from 'features/common/slices/api/index';

function AddListingTerms({ form, buttons }) {
  const dispatch = useDispatch();
  const isLoading = useSelector(checkIfIsLoading);
  const [contract, setContract] = useState(null);

  const generateContractPreview = async () => {
    const response = await dispatch(getContractPreviewThunk(form.values.contract));
    if (response.payload.success){
      setContract(response.payload.data)
    }
  };

  return (
    <form onSubmit={form.handleSubmit}>
      <Grid container columns={12} columnSpacing={2} rowSpacing={2}>
        <Grid item xs={12}>
          <Typography variant="subtitle2">Contract information</Typography>
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="contract.price"
            label="Price"
            InputProps={{
              startAdornment: <InputAdornment position="start">$</InputAdornment>,
              inputMode: 'numeric',
              pattern: '[0-9]*',
            }}
            fullWidth
            error={form.errors.contract?.price && Boolean(form.errors.contract?.price)}
            helperText={form.touched.contract?.price && form.errors.contract?.price}
            onChange={form.handleChange}
            value={form.values.contract.price}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="contract.fullName"
            label="Full name"
            fullWidth
            error={form.errors.contract?.fullName && Boolean(form.errors.contract?.fullName)}
            helperText={form.touched.contract?.fullName && form.errors.contract?.fullName}
            onChange={form.handleChange}
            value={form.values.contract.fullName}
          />
        </Grid>
        <Grid item xs={4}>
          <DesktopDatePicker
            name="contract.date"
            label="Date"
            inputFormat="MM/dd/yyyy"
            renderInput={(params) => (
              <TextField
                fullWidth
                {...params}
                error={form.errors.contract?.date && Boolean(form.errors.contract?.date)}
              />
            )}
            onChange={(value) => form.setFieldValue('contract.date', value)}
            value={form.values.contract?.date}
          />
          <FormHelperText error>
            {form.touched.contract?.date && form.errors.contract?.date}
          </FormHelperText>
        </Grid>
        <Grid item xs={12}>
          <Button type="button" variant="contained" onClick={generateContractPreview}>
            Generate contract preview
          </Button>
        </Grid>
        {contract && <Grid item xs={12}>
          <Card variant="outlined" >
              <CardContent dangerouslySetInnerHTML={{ __html: contract }} />
          </Card>
        </Grid>}
        <Grid item xs={12}>
          {buttons &&
            buttons.map((button) => {
              const ButtonComponent = button.withLoader ? LoadingButton : Button;
              return <ButtonComponent loading={isLoading} onClick={button.onClick} type={button.type} variant="secondary">
                {button.text}
              </ButtonComponent>
              }
            )}
        </Grid>
      </Grid>
    </form>
  );
}

AddListingTerms.displayName = 'AddListingTerms';

export default AddListingTerms;
