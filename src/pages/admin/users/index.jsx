import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';

const UsersPage = loadable(() => import(/* webpackChunkName: "buyer-page" */ './page'), {
  resolveComponent: ({ UsersPage: Page }) => Page,
  fallback: <Loading />,
});

export default UsersPage;
