import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import FormHelperText from '@mui/material/FormHelperText';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import Typography from '@mui/material/Typography';
import { termsThunk } from 'features/common/thunks/auth';

export default function Disclosures({ form }) {
  const [modalContent, setModalContent] = useState(null);
  const [terms, setTerms] = useState(null);
  const [policy, setPolicy] = useState(null);
  const [service, setService] = useState(null);

  const dispatch = useDispatch();

  useEffect(async () => {
    const { payload } = await dispatch(termsThunk());
    if (payload.success) {
      setTerms(payload.data.find(item => item.name === 'terms_conditions').text);
      setPolicy(payload.data.find(item => item.name === 'privacy_policy').text);
      setService(payload.data.find(item => item.name === 'terms_service').text);
    }
  }, []);

  return (
    <div>
      <form onSubmit={form.handleSubmit}>
        <Grid container columns={12} spacing={1} justifyContent="center">
          <Grid item xs={6}>
            <Typography variant="h5">Terms and Agreements</Typography>
            {terms && <p dangerouslySetInnerHTML={{ __html: terms.substring(terms.indexOf('<p>'), terms.indexOf('</p>')) + '...'}}/>}
            <Button onClick={() => setModalContent(terms)}>Read more</Button>
          </Grid>
          <Grid item xs={1}>
            <FormControlLabel
              control={<Checkbox name="termsAgreed" onChange={form.handleChange} />}
              label="Agree"
              checked={form.values.termsAgreed}
            />
            <FormHelperText error>{form.errors.termsAgreed}</FormHelperText>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="h5">Privacy Policy</Typography>
            {policy && <p dangerouslySetInnerHTML={{ __html: policy.substring(policy.indexOf('<p>'), policy.indexOf('</p>')) + '...'}}/>}
            <Button onClick={() => setModalContent(policy)}>Read more</Button>
          </Grid>
          <Grid item xs={1}>
            <FormControlLabel
              control={<Checkbox name="privacyPolicyAgreed" onChange={form.handleChange} />}
              label="Agree"
              checked={form.values.privacyPolicyAgreed}
            />
            <FormHelperText error>{form.errors.privacyPolicyAgreed}</FormHelperText>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="h5">Terms and Service</Typography>
            {service && <p dangerouslySetInnerHTML={{ __html: service.substring(service.indexOf('<p>'), service.indexOf('</p>')) + '...'}}/>}
            <Button onClick={() => setModalContent(service)}>Read more</Button>
          </Grid>
          <Grid item xs={1}>
            <FormControlLabel
              control={<Checkbox name="serviceAgreed" onChange={form.handleChange} />}
              label="Agree"
              checked={form.values.serviceAgreed}
            />
            <FormHelperText error>{form.errors.serviceAgreed}</FormHelperText>
          </Grid>
        </Grid>
      </form>
      <Dialog onClose={() => setModalContent(null)} open={!!modalContent}>
        <DialogContent>
          <p dangerouslySetInnerHTML={{ __html: modalContent }}/>  
        </DialogContent>
      </Dialog>
    </div>
  );
}

Disclosures.displayName = 'Disclosures';

Disclosures.propTypes = {
  form: PropTypes.shape({
    errors: PropTypes.shape({
      privacyPolicyAgreed: PropTypes.string,
      serviceAgreed: PropTypes.string,
      termsAgreed: PropTypes.string,
    }),
    handleChange: PropTypes.func,
    handleSubmit: PropTypes.func,
    values: PropTypes.shape({
      privacyPolicyAgreed: PropTypes.bool,
      serviceAgreed: PropTypes.bool,
      termsAgreed: PropTypes.bool,
    }),
  }).isRequired,
};
