import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { TextField, Grid } from '@mui/material';
import { toast } from 'react-toastify';
import { getCities, getStates, getTitlesRequest } from 'features/profile/api/index';
import CircularProgress from '@mui/material/CircularProgress';
import MenuItem from '@mui/material/MenuItem';
import capitalize from 'lodash/capitalize';
import Select from '@mui/material/Select';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import InputLabel from '@mui/material/InputLabel';

function AccountInformation({ form }) {
  const [attemptTitle, setAttemptTitle] = useState(false);
  const [loadingTitle, setLoadingTitle] = useState(false);
  const [titleOptions, setTitlesOptions] = useState(['mr', 'mrs']);

  const [states, setStates] = useState(null);
  const [cities, setCities] = useState(null);

  useEffect(async () => {
    const { data } = await getStates();
    setStates(data.states);
  }, []);

  const handleOpenTitleSelect = async () => {
    try {
      if (!attemptTitle) {
        setLoadingTitle(true);
        setAttemptTitle(true);
        const { data } = await getTitlesRequest();

        setTitlesOptions(data);
      }
    } catch (error) {
      toast.error(error.message);
    } finally {
      setLoadingTitle(false);
    }
  };

  const handleOpenCitySelect = async () => {
    const stateId = states.find((state) => state.name === form.values.homeAddress.state).id;
    const { data } = await getCities(stateId);
    setCities(data.cities);
  };

  return (
    <form onSubmit={form.handleSubmit}>
      <Grid container columns={12} columnSpacing={2} rowSpacing={4}>
        <Grid item xs={6}>
          <TextField
            name="user.firstName"
            label="First name"
            fullWidth
            disabled
            error={form.errors.user?.firstName && Boolean(form.errors.user?.firstName)}
            helperText={form.touched.user?.firstName && form.errors.user?.firstName}
            onChange={form.handleChange}
            value={form.values.user.firstName}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="user.lastName"
            label="Last name"
            fullWidth
            disabled
            error={form.errors.user?.lastName && Boolean(form.errors.user?.lastName)}
            helperText={form.touched.user?.lastName && form.errors.user?.lastName}
            onChange={form.handleChange}
            value={form.values.user.lastName}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="profile.company"
            label="Company name"
            fullWidth
            error={form.errors.profile?.company && Boolean(form.errors.profile?.company)}
            helperText={form.touched.profile?.company && form.errors.profile?.company}
            onChange={form.handleChange}
            value={form.values.profile.company}
          />
        </Grid>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <InputLabel>Title</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="profile.title"
              onOpen={handleOpenTitleSelect}
              label="Title"
              error={form.errors.profile?.title && Boolean(form.errors.profile?.title)}
              onChange={form.handleChange}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() =>
                loadingTitle ? (
                  <CircularProgress />
                ) : (
                  <i className="material-icons">arrow_drop_down</i>
                )
              }
              value={String(form.values.profile?.title)}
            >
              <MenuItem disabled value="">
                <span>Title</span>
              </MenuItem>
              {titleOptions.map((title) => (
                <MenuItem key={title} value={title}>
                  {capitalize(title)}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText error>
              {form.touched.profile?.title && form.errors.profile?.title}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="homePhone.number"
            label="Phone"
            fullWidth
            type="phone"
            error={form.errors.homePhone?.number && Boolean(form.errors.homePhone?.number)}
            helperText={form.touched.homePhone?.number && form.errors.homePhone?.number}
            onChange={form.handleChange}
            value={form.values.homePhone.number}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="user.email"
            label="Email"
            type="email"
            fullWidth
            disabled
            error={form.errors.user?.email && Boolean(form.errors.user?.email)}
            helperText={form.touched.user?.email && form.errors.user?.email}
            onChange={form.handleChange}
            value={form.values.user.email}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="homeAddress.lineOne"
            label="Address line 1"
            fullWidth
            error={form.errors.homeAddress?.lineOne && Boolean(form.errors.homeAddress?.lineOne)}
            helperText={form.touched.homeAddress?.lineOne && form.errors.homeAddress?.lineOne}
            onChange={form.handleChange}
            value={form.values.homeAddress.lineOne}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="homeAddress.lineTwo"
            label="Address line 2"
            fullWidth
            error={form.errors.homeAddress?.lineTwo && Boolean(form.errors.homeAddress?.lineTwo)}
            helperText={form.touched.homeAddress?.lineTwo && form.errors.homeAddress?.lineTwo}
            onChange={form.handleChange}
            value={form.values.homeAddress.lineTwo}
          />
        </Grid>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <InputLabel>State</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="homeAddress.state"
              label="State"
              error={form.errors.homeAddress?.state && Boolean(form.errors.homeAddress?.state)}
              onChange={(e) => {
                form.setFieldValue('homeAddress.city', '');
                form.handleChange(e);
              }}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.homeAddress?.state)}
            >
              <MenuItem disabled value="">
                <span>State</span>
              </MenuItem>
              {states &&
                states.map((state) => (
                  <MenuItem key={state.id} value={state.name}>
                    {capitalize(state.name)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.homeAddress?.state && form.errors.homeAddress?.state}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <FormControl fullWidth>
            <InputLabel>City</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="homeAddress.city"
              onOpen={handleOpenCitySelect}
              label="City"
              disabled={!form.values.homeAddress.state}
              error={form.errors.homeAddress?.city && Boolean(form.errors.homeAddress?.city)}
              onChange={form.handleChange}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.homeAddress?.city)}
            >
              <MenuItem disabled value="">
                <span>City</span>
              </MenuItem>
              {cities &&
                cities.map((city) => (
                  <MenuItem key={city.id} value={city.name}>
                    {capitalize(city.name)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.homeAddress?.city && form.errors.homeAddress?.city}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={6}>
          <TextField
            name="homeAddress.zipCode"
            label="Zip code"
            fullWidth
            error={form.errors.homeAddress?.zipCode && Boolean(form.errors.homeAddress?.zipCode)}
            helperText={form.touched.homeAddress?.zipCode && form.errors.homeAddress?.zipCode}
            onChange={form.handleChange}
            value={form.values.homeAddress.zipCode}
          />
        </Grid>
      </Grid>
    </form>
  );
}

AccountInformation.displayName = 'AccountInformation';

AccountInformation.propTypes = {
  form: PropTypes.shape({
    errors: PropTypes.shape({
      homeAddress: PropTypes.shape({
        city: PropTypes.string,
        lineOne: PropTypes.string,
        lineTwo: PropTypes.string,
        state: PropTypes.string,
        zipCode: PropTypes.string,
      }),
      homePhone: PropTypes.shape({
        number: PropTypes.string,
      }),
      profile: PropTypes.shape({
        company: PropTypes.string,
        title: PropTypes.string,
      }),
      user: PropTypes.shape({
        email: PropTypes.string,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
      }),
    }),
    handleChange: PropTypes.func,
    handleSubmit: PropTypes.func,
    setFieldValue: PropTypes.func,
    touched: PropTypes.shape({
      homeAddress: PropTypes.shape({
        city: PropTypes.bool,
        lineOne: PropTypes.bool,
        lineTwo: PropTypes.bool,
        state: PropTypes.bool,
        zipCode: PropTypes.bool,
      }),
      homePhone: PropTypes.shape({
        number: PropTypes.bool,
      }),
      profile: PropTypes.shape({
        company: PropTypes.bool,
        title: PropTypes.bool,
      }),
      user: PropTypes.shape({
        email: PropTypes.bool,
        firstName: PropTypes.bool,
        lastName: PropTypes.bool,
      }),
    }),
    values: PropTypes.shape({
      homeAddress: PropTypes.shape({
        city: PropTypes.string,
        lineOne: PropTypes.string,
        lineTwo: PropTypes.string,
        state: PropTypes.string,
        zipCode: PropTypes.string,
      }),
      homePhone: PropTypes.shape({
        number: PropTypes.string,
      }),
      profile: PropTypes.shape({
        company: PropTypes.string,
        title: PropTypes.string,
      }),
      user: PropTypes.shape({
        email: PropTypes.string,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
      }),
    }),
  }).isRequired,
};

export default AccountInformation;
